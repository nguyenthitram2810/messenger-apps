import methodOverride from 'method-override';
import routes from '../routes/index';
import localHelpers from '../infrastructure/Helpers/localHelpers'

export default function (app) {
  app.use(methodOverride('X-HTTP-Method-Override'));

  app.use(
    methodOverride((req) => {
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        const method = req.body._method;
        delete req.body._method;

        return method;
      }

      return undefined;
    }),
  );

  app.use(async (req, res, next) => {
    res.redirectBack = () => {
      const backURL = req.header('Referer') || '/';
      return res.redirect(backURL);
    };
    // new
    localHelpers(res);
    next();
  });

  app.use(routes);
}
