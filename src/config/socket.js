import socket from 'socket.io';
import ConversationService from '../app/Conversation/Services/ConversationService';

export default function (server) {
    // const server = http.createServer(app);
    const io = socket(server);

    ConversationService.io = io.of('/conversations').on('connection', (client) => {
        client.on('join', (data) => {
            console.log(`DA join${data}`);
            client.join(data);
        });
    });

    ConversationService.notifyIO= io.of('/userConversation').on('connection', (client) => {
        client.on('join', (data) => {
            console.log(`DA join${data}`);
            client.join(data);
        });
    });
}
