import express from 'express';
import authRouter from '../app/Auth/Routes/routes';
import middleware from '../app/Auth/Middleware/AuthMiddleware';
import conversationRouter from '../app/Conversation/Routes/routes';
import ConversationController from '../app/Conversation/Controllers/ConversationController';


const router = express.Router();
const conversationController = new ConversationController();
router.use(authRouter);
router.use(conversationRouter);

router.get('/', middleware.verifyAuthentication, conversationController.callMethod('indexPage'));

export default router;

// (req, res) => res.render('app/conversation/index')
