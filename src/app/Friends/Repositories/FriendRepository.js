/* eslint-disable no-param-reassign */
import BaseRepository from '../../../infrastructure/Repositories/BaseRepository';

class FriendRepository extends BaseRepository {
  static repository;

  static getRepository() {
    if (!this.repository) {
      this.repository = new this();
    }

    return this.repository;
  }

  getTableName() {
    return 'friends';
  }

  addFriend(data) {
    return this.insertData({
      friendId: data.receiverID,
      receiverId: data.receiverID,
      userId: data.userID,
      status: '0',
    });
  }

  doubleFriend(data) {
    return this.insertData({
      friendId: data.userID,
      receiverId: data.receiverID,
      userId: data.receiverID,
      status: '0',
    });
  }

  listRequest(userID) {
    return this.cloneQuery().where({
      userId: userID,
      receiverId: userID,
      status: 0,
    }).select('*').leftJoin('users', 'friends.friendId', 'users.id');
  }

  acceptFriend(data) {
    return this.cloneQuery().where({
      userId: data.userID,
      friendId: data.friendID,
    }).orWhere({
      userId: data.friendID,
      friendId: data.userID,
    }).update({ status: 1 });
  }

  denyFriend(userId, friendId) {
    return this.delete({
      userId,
      friendId,
    });
  }
}

export default FriendRepository;
