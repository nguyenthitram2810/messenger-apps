/* eslint-disable no-useless-escape */
/* eslint-disable no-param-reassign */
import friendRepository from '../Repositories/FriendRepository';
import Repository from '../../Auth/Repositories/AuthRepository';

// const getSlug = require('speakingurl');

class FriendService {
  static service;

  constructor() {
    this.repository = friendRepository.getRepository();
    this.authRepository = Repository.getRepository();
  }

  static getService() {
    if (!this.service) {
      this.service = new this();
    }
    return this.service;
  }

  async addFriend(data, userID, res) {
    data.userID = userID;
    const reEmail = /^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$/;
    const reNum = /((09|03|07|08|05)+([0-9]{8})\b)/g;
    if (reEmail.test(String(data.info))) {
      data.type = 'email';
      const receiverID = await this.authRepository.getUserID(data);
      data.receiverID = receiverID.id;
      await this.repository.addFriend(data);
      await this.repository.doubleFriend(data);
      return res.json('successful');
    } if (reNum.test(String(data.info))) {
      let phoneNumber = data.info;
      phoneNumber = `+84${phoneNumber.substring(1, phoneNumber.length)}`;
      data.info = phoneNumber;
      data.type = 'phone';
      const receiverID = await this.authRepository.getUserID(data);
      data.receiverID = receiverID.id;
      await this.repository.addFriend(data);
      await this.repository.doubleFriend(data);
      return res.json('successful');
    }
      return res.json('fail');
  }

  listRequest(userID) {
    return this.repository.listRequest(userID);
  }

  async acceptFriend(data) {
    await this.repository.acceptFriend(data);
  }

  async denyFriend(data) {
    const userId = data.userID;
    const friendId = data.friendID;
    await this.repository.denyFriend(userId, friendId);
    await this.repository.denyFriend(friendId, userId);
  }
}

export default FriendService;
