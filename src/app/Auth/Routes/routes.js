import express from 'express';
import path from 'path';
import multer from 'multer';
import Controller from '../Controllers/AuthController';
import middleware from '../Middleware/AuthMiddleware';
import FriendsController from '../../Friends/Controllers/FriendController';

const router = express.Router();
const controller = new Controller();
const friendsController = new FriendsController();
// const storage = multer.diskStorage({
//     destination(req, file, cb) {
//         cb(null, 'uploads');
//     },
//     filename(req, file, cb) {
//         cb(null, `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`);
//     },
// });

// const upload = multer({ storage });

// ------login & register
router.get('/register', middleware.verifyNotAuthtication, (req, res) => res.render('app/auth/register'));
router.get('/login', middleware.verifyNotAuthtication, (req, res) => res.render('app/login'));
// email
router.get('/login-email', middleware.verifyNotAuthtication, (req, res) => res.render('app/login-email'));
router.post('/login-email', middleware.checkIDToken, controller.callMethod('loginEmailMethod'));
router.get('/register-email', middleware.verifyNotAuthtication, (req, res) => res.render('app/auth/register-email'));
router.post('/register-email', controller.callMethod('registerMethod'));
router.get('/reset-password', (req, res) => res.render('app/reset-password'));
// sms
router.get('/login-sms', middleware.verifyNotAuthtication, (req, res) => res.render('app/login-sms'));
router.post('/login-sms', middleware.checkIDToken, controller.callMethod('loginSMSMethod'));
router.get('/register-sms', middleware.verifyNotAuthtication, (req, res) => res.render('app/auth/register-sms'));
router.post('/register-sms', controller.callMethod('registerSMS'));
// user
// eslint-disable-next-line max-len
// router.post('/editUser', upload.single('avatar'), controller.callMethod('editUser'));
router.post('/add-friend', friendsController.callMethod('addFriend'));
router.post('/accept-friend', friendsController.callMethod('acceptFriend'));
router.post('/deny-friend', friendsController.callMethod('denyFriend'));

// router.post('/search-user', middleware.verifyAuthentication, controller.callMethod('searchUser'));
router.get('/logout', (req, res) => {
    delete req.session.user;
    return res.redirect('/login');
});


router.post('/get-user-info', middleware.verifyAuthentication, controller.callMethod('getUserInfoMethod'));
// , upload.single('file')
export default router;
