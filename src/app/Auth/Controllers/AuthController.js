import BaseController from '../../../infrastructure/Controllers/BaseController';
import Service from '../Services/AuthService';

class AuthController extends BaseController {
  constructor() {
    super();
    this.service = Service.getService();
  }

  registerMethod(req, res) {
    const data = req.body;
    this.service.register(data);
    res.json('successful');
  }

  async registerSMS(req, res) {
    const data = req.body;
    const user = await this.service.registerSMS(data);
    if (user) {
      req.session.user = user;
    }
    res.json('successful');
  }

  async loginEmailMethod(req, res) {
    const data = req.body;
    const user = await this.service.loginEmail(data);
    if (user) {
      req.session.user = user;
    }
    res.json('successful');
  }

  async loginSMSMethod(req, res) {
    const data = req.body;
    const user = await this.service.loginSMS(data);
    if (user) {
      req.session.user = user;
    }
    res.json('successful');
  }

  // editUser(req, res) {
  //   const data = req.body;
  //   const images = req.file;
  //   console.log(data);
  //   //this.service.editUser(data, req);
  //   res.json('successful');
  // }

  async getUserInfoMethod(req, res) {
    const data = req.body;
    const result = await this.service.getUserInfoService(data);
    return res.json(result);
  }

  // async searchUser(req, res) {
  //   const data = req.body;
  //   const users = await this.service.SearchByName(data);
  //   return res.json(users);
  // }
}

export default AuthController;
