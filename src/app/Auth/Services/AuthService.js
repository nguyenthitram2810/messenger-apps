import Repository from '../Repositories/AuthRepository';
import { thisExpression } from 'babel-types';

const getSlug = require('speakingurl');
// const repository = new Repository();
class AuthService {
  static service;

  constructor() {
    this.repository = Repository.getRepository();
  }

  static getService() {
    if (!this.service) {
      this.service = new this();
    }
    return this.service;
  }

  async register(data) {
    // eslint-disable-next-line no-param-reassign
    data.slug = getSlug(`${data.lastName}-${Date.now()}`);
    await this.repository.registerDB(data);
    const infoMongo = await this.repository.getInfo(data);
    await this.repository.CreateMongo(infoMongo);
  }

  registerSMS(data) {
    // eslint-disable-next-line no-param-reassign
    data.slug = getSlug(`${data.lastName}-${Date.now()}`);
    return this.repository.registerSMS(data);
  }

  loginEmail(data) {
    return this.repository.checkLoginEmail(data);
  }

  loginSMS(data) {
    return this.repository.checkLoginPhone(data);
  }

  async getUserInfoService(data) {
    let result = await this.repository.getInfo(data);
    result = {
      id: result.id,
      email: result.email,
      lastName: result.lastName,
    };
    return result;
  }

  // SearchByName(data) {
  //   return this.repository.getFullName(data);
  // }
  // async editUser(data, req) {
  //   await this.repository.editUser(data, req);
  // }
}

export default AuthService;
