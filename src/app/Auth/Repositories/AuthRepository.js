/* eslint-disable no-param-reassign */
import BaseRepository from '../../../infrastructure/Repositories/BaseRepository';
import model from '../../../database/models/index';

class AuthRepository extends BaseRepository {
  static repository;

  static getRepository() {
    if (!this.repository) {
      this.repository = new this();
    }

    return this.repository;
  }

  getTableName() {
    return 'users';
  }

  registerDB(data) {
    data.fullName = `${data.lastName} ${data.firstName}`;
    return this.insertData(data);
  }

  registerSMS(data) {
    data.fullName = `${data.lastName} ${data.firstName}`;
    return this.create(data);
  }

  checkLoginEmail(data) {
    return this.getBy({
      email: data.email,
      password: data.password,
    });
  }

  checkLoginPhone(data) {
    return this.getBy({
      phoneNumber: data.phoneNumber,
    });
  }

  // editUSer(data, req) {
  //   const clauses = '';
  //   if (req.session.user.phoneNumber) {
  //     // eslint-disable-next-line no-const-assign
  //     clauses = req.session.user.phoneNumber;
  //   }
  //   if (req.session.user) {
  //     // eslint-disable-next-line no-const-assign
  //     clauses = req.session.user.email;
  //   }
  //   return this.update(clauses, data);
  // }

  getUserID(data) {
    if (data.type === 'email') {
      return this.getBy({
        email: data.info,
      });
    }
    if (data.type === 'phone') {
      return this.getBy({
        phoneNumber: data.info,
      });
    }
  }


  getInfo(data) {
    return this.getBy({
      email: data.email,
    });
  }

  getInfoSMS(data) {
    return this.getBy({
      phoneNumber: data.phoneNumber,
    });
  }

  getFullName(data) {
    return this.cloneQuery().where('fullName', 'like', `%${data.fullname}%`).select('*');
  }

  CreateMongo(data) {
    return model.User.create({
      id: data.id,
      name: data.fullName,
      avatar: data.avatar,
    });
  }
}

export default AuthRepository;
