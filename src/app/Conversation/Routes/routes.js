import express from 'express';
import ConversationController from '../Controllers/ConversationController';
import FriendController from '../../Friends/Controllers/FriendController';
import middleware from '../../Auth/Middleware/AuthMiddleware';

const router = express.Router();
// Declare Object
const conversationController = new ConversationController();

router.post('/group-add', middleware.verifyAuthentication, conversationController.callMethod('groupAddMethod'));
router.post('/search-user', middleware.verifyAuthentication, conversationController.callMethod('searchUser'));
router.post('/list-conversation', middleware.verifyAuthentication, conversationController.callMethod('listConversation'));

router.get('/:slug', middleware.verifyAuthentication, conversationController.callMethod('listMessage'));
router.post('/send-message', middleware.verifyAuthentication, conversationController.callMethod('sendMessage'));
// router.get('/t/:slug', middleware.verifyAuthentication, conversationController.callMethod('listMessage'));
export default router;
