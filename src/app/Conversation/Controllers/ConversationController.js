import BaseController from '../../../infrastructure/Controllers/BaseController';
import Service from '../Services/ConversationService';

class ConversationController extends BaseController {
  constructor() {
    super();
    this.service = Service.getService();
  }

  async indexPage(req, res) {
    const ID = req.session.user.id;
    const conversations = await this.service.listConversation(ID);
    console.log(typeof (conversations));
    res.render('app/conversation/index', { conversations, ID });
  }

  groupAddMethod(req, res) {
    const data = req.body;
    data.users_ID = JSON.parse(data.users_ID);
    data.users_objId = JSON.parse(data.users_objId);
    this.service.groupAddService(data);
    res.json('successful');
  }

  async searchUser(req, res) {
    const ID = req.session.user.id;
    const data = req.body;
    data.fullName = JSON.parse(data.fullName);
    const user = await this.service.searchUser(data);
    const objId = await this.service.yourObjId(ID);
    return res.json({ user, objId });
  }

  async listConversation(req, res) {
    const ID = req.session.user.id;
    const conversations = await this.service.listConversation(ID);
    return res.json({ conversations, ID });
  }

  sendMessage(req, res) {
    const ID = req.session.user.id;
    const data = req.body;
    data.mem = JSON.parse(data.mem);
    this.service.sendMessage(data, ID);
    return res.json('successful');
  }

  async listMessage(req, res) {
    const ID = req.session.user.id;
    const roomID = req.params.slug;
    const conversations = await this.service.listConversation(ID);
    const messages = await this.service.listMessage(roomID);
    res.render('app/conversation/index', { conversations, ID, messages });
  }
}

export default ConversationController;
