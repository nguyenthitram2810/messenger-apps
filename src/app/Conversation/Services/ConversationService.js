import ConversationRepository from '../Repositories/ConversationRepository';

class ConversationService {
  static service;

  static io;

  static notifyIO;

  constructor() {
    this.repository = ConversationRepository.getRepository();
  }

  static getService() {
    if (!this.service) {
      this.service = new this();
    }
    return this.service;
  }

  async groupAddService(data) {
    await this.repository.groupAdd(data);
  }

  async searchUser(data) {
    const user = await this.repository.searchUser(data);
    return user;
  }

  async yourObjId(ID) {
    const objId = await this.repository.yourObjId(ID);
    return objId;
  }

  async listConversation(ID) {
    const objId = await this.repository.yourObjId(ID);
    const list = this.repository.listConversation(objId);
    return list;
  }

  async sendMessage(data, ID) {
    const { message, roomID, mem } = data;
    const objId = await this.repository.yourObjId(ID);
    await this.repository.saveMessage(message, roomID, objId);
    await this.repository.updateConversation(roomID, message);
    const conversations = await this.repository.listConversation(objId);
    mem.forEach((m) => {
      if (m.id !== ID) {
        ConversationService.notifyIO.to(m.id).emit('notifyMessage',
        {
          message, ID, conversations, roomID,
        });
      }
    });
    ConversationService.io.to(data.roomID).emit('newMessage',
    {
      message, ID, conversations, roomID,
    });
  }

  listMessage(roomID) {
    return this.repository.listMessage(roomID);
  }
}

export default ConversationService;
