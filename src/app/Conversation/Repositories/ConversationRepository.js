import BaseRepository from '../../../infrastructure/Repositories/BaseRepository';

import model from '../../../database/models/index';

class ConversationRepository extends BaseRepository {
  static repository;

  static getRepository() {
    if (!this.repository) {
      this.repository = new this();
    }
    return this.repository;
  }

  getTableName() {
    return 'conversations';
  }

  groupAdd(data) {
    model.Conversation.create({
      groupName: data.groupName,
      member: data.users_ID,
      description: data.description,
      userIds: data.users_objId,
    });
  }

  async yourObjId(ID) {
    const query = await model.User.find({
      id: ID,
    });
    return query;
  }

  async searchUser(data) {
    const query = await model.User.find({
      name: { $regex: data.fullName[0], $options: 'i' },
    });
    return query;
  }

  listConversation(objId) {
    return model.Conversation
    .find({ userIds: { $elemMatch: { $eq: objId[0]._id } } })
    .select('groupName description _id updatedAt createdAt')
    .populate('userIds', 'name avatar id -_id')
    .sort({ updatedAt: -1 });
  }

  saveMessage(message, roomID, objId) {
    return model.Message.create({
      content: message,
      conversationID: roomID,
      memberId: objId,
    });
  }

  listMessage(roomID) {
    return model.Message
    .find({ conversationID: roomID })
    .select('content memberId _id conversationID createdAt updatedAt')
    .populate('conversationID', 'userIds')
    .populate('memberId', 'name avatar id -_id');
  }

  updateConversation(roomID, message) {
    return model.Conversation
    .find({ _id: roomID })
    .update({ description: message });
  }
}

export default ConversationRepository;
