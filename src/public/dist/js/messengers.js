$(function () {
    $('#chatBar').submit(function (e) {
        e.preventDefault();
        let mem = new Array;
        const chat_body = $('.layout .content .chat .chat-body');
        const roomID = $('.open-chat').data('conversation-id');
        conversations.forEach(function(u) {
          if(u._id == roomID) {
            u.userIds.forEach(function(s) {
              mem.push(s);
            })
          }
        })
        let message = $('input[name="messages"]').val(); 
        $('input[name="messages"]').val('');
        message = $.trim(message);
        if (message) {
          $('.layout .content .chat .chat-body .messages').append('<div class="message-item outgoing-message ' + '"><div class="message-content">' + message + '</div><div class="message-action">PM 14:25 ' + ('<i class="ti-check"></i>') + '</div></div>');
          
          chat_body.scrollTop(chat_body.get(0).scrollHeight, -1).niceScroll({
            cursorcolor: 'rgba(66, 66, 66, 0.20)',
            cursorwidth: "4px",
            cursorborder: '0px'
          }).resize();
          mem = JSON.stringify(mem);
          $.ajax({
            type:'POST',
            url:'/send-message',
            data: {
              roomID,
              message,
              mem,
            },
            success: function(data) {
              
            },
          });
        }
    });

    socket.on('newMessage', (data) => {   
      let userID = $('#Itsme').data('user');
      let incomingMessage = data.message;
      if(data.ID !== userID) {
        $('#conversations').remove();
      let html = "<ul id = 'conversations' class = 'list-group list-group-flush'>";
        conversations.forEach(function(dt) {
          if(dt.userIds.length == 2) {
            if (dt._id == data.roomID)
              dt.userIds.forEach(function(ob) {
                if(data.ID != ob.id ) {
                   html = html + "<li class='list-group-item conversation open-chat' data-conversation-id='"+ dt._id+"'><div><figure class='avatar'><img class='rounded-circle' src='dist/images/women_avatar5.jpg'></figure></div><div class='users-list-body'><h5>" + ob.name + "</h5><p>" + dt.description + "</p><div class='users-list-action action-toggle'><div class='dropdown'><a data-toggle='dropdown' href='#'><i class='ti-more'></i></a><div class='dropdown-menu dropdown-menu-right'><a class='dropdown-item' href='#'>Open</a><a class='dropdown-item active' href='#' data-navigation-target='contact-information'>Profile</a><a class='dropdown-item' href='#'>Add to archive</a><a class='dropdown-item' href='#'>Delete</a></div></div></div></div></li>";
                }
              })
            else 
              dt.userIds.forEach(function(ob) {
                if(data.ID != ob.id ) {
                   html = html + "<li class='list-group-item conversation' data-conversation-id='"+ dt._id+"'><div><figure class='avatar'><img class='rounded-circle' src='dist/images/women_avatar5.jpg'></figure></div><div class='users-list-body'><h5>" + ob.name + "</h5><p>" + dt.description + "</p><div class='users-list-action action-toggle'><div class='dropdown'><a data-toggle='dropdown' href='#'><i class='ti-more'></i></a><div class='dropdown-menu dropdown-menu-right'><a class='dropdown-item' href='#'>Open</a><a class='dropdown-item active' href='#' data-navigation-target='contact-information'>Profile</a><a class='dropdown-item' href='#'>Add to archive</a><a class='dropdown-item' href='#'>Delete</a></div></div></div></div></li>";
                }
              })
          }
          else {
            if (dt._id == data.roomID)
              html = html + "<li class='list-group-item conversation open-chat' data-conversation-id='"+ dt._id+"'><div><figure class='avatar'><img class='rounded-circle' src='dist/images/women_avatar5.jpg'></figure></div><div class='users-list-body'><h5>" + dt.groupName + "</h5><p>" + dt.description + "</p><div class='users-list-action action-toggle'><div class='dropdown'><a data-toggle='dropdown' href='#'><i class='ti-more'></i></a><div class='dropdown-menu dropdown-menu-right'><a class='dropdown-item' href='#'>Open</a><a class='dropdown-item active' href='#' data-navigation-target='contact-information'>Profile</a><a class='dropdown-item' href='#'>Add to archive</a><a class='dropdown-item' href='#'>Delete</a></div></div></div></div></li>";
            else
              html = html + "<li class='list-group-item conversation' data-conversation-id='"+ dt._id+"'><div><figure class='avatar'><img class='rounded-circle' src='dist/images/women_avatar5.jpg'></figure></div><div class='users-list-body'><h5>" + dt.groupName + "</h5><p>" + dt.description + "</p><div class='users-list-action action-toggle'><div class='dropdown'><a data-toggle='dropdown' href='#'><i class='ti-more'></i></a><div class='dropdown-menu dropdown-menu-right'><a class='dropdown-item' href='#'>Open</a><a class='dropdown-item active' href='#' data-navigation-target='contact-information'>Profile</a><a class='dropdown-item' href='#'>Add to archive</a><a class='dropdown-item' href='#'>Delete</a></div></div></div></div></li>";
            }
        })
        html = html + "</ul>";
        $('#list').append(html);
      }
      const chat_body = $('.layout .content .chat .chat-body');
      if (data.ID !== userID) {
        $('.layout .content .chat .chat-body .messages').append('<div class="message-item ' + '"><div class="message-content">' + incomingMessage + '</div><div class="message-action">PM 14:25 ' + ('<i class="ti-check"></i>') + '</div></div>');
      }
      chat_body.scrollTop(chat_body.get(0).scrollHeight, -1).niceScroll({
        cursorcolor: 'rgba(66, 66, 66, 0.20)',
        cursorwidth: "4px",
        cursorborder: '0px'
      }).resize();
    });

    socketUser.on('notifyMessage', (data) => {   
      let incomingMessage = data.message;
      console.log(conversations);
      $('#conversations').remove();

      let html = "<ul id = 'conversations' class = 'list-group list-group-flush'>";
        data.conversations.forEach(function(dt) {

          if(dt.userIds.length == 2) {
            if (dt._id == data.roomID)
              dt.userIds.forEach(function(ob) {
                if(data.ID != ob.id ) {
                   html = html + "<li class='list-group-item conversation open-chat' data-conversation-id='"+ dt._id+"'><div><figure class='avatar'><img class='rounded-circle' src='dist/images/women_avatar5.jpg'></figure></div><div class='users-list-body'><h5>" + ob.name + "</h5><p>" + dt.description + "</p><div class='users-list-action action-toggle'><div class='dropdown'><a data-toggle='dropdown' href='#'><i class='ti-more'></i></a><div class='dropdown-menu dropdown-menu-right'><a class='dropdown-item' href='#'>Open</a><a class='dropdown-item active' href='#' data-navigation-target='contact-information'>Profile</a><a class='dropdown-item' href='#'>Add to archive</a><a class='dropdown-item' href='#'>Delete</a></div></div></div></div></li>";
                }
              })
            else 
              dt.userIds.forEach(function(ob) {
                if(data.ID != ob.id ) {
                   html = html + "<li class='list-group-item conversation' data-conversation-id='"+ dt._id+"'><div><figure class='avatar'><img class='rounded-circle' src='dist/images/women_avatar5.jpg'></figure></div><div class='users-list-body'><h5>" + ob.name + "</h5><p>" + dt.description + "</p><div class='users-list-action action-toggle'><div class='dropdown'><a data-toggle='dropdown' href='#'><i class='ti-more'></i></a><div class='dropdown-menu dropdown-menu-right'><a class='dropdown-item' href='#'>Open</a><a class='dropdown-item active' href='#' data-navigation-target='contact-information'>Profile</a><a class='dropdown-item' href='#'>Add to archive</a><a class='dropdown-item' href='#'>Delete</a></div></div></div></div></li>";
                }
              })
          }
          else {
            if (dt._id == data.roomID)
              html = html + "<li class='list-group-item conversation open-chat' data-conversation-id='"+ dt._id+"'><div><figure class='avatar'><img class='rounded-circle' src='dist/images/women_avatar5.jpg'></figure></div><div class='users-list-body'><h5>" + dt.groupName + "</h5><p>" + dt.description + "</p><div class='users-list-action action-toggle'><div class='dropdown'><a data-toggle='dropdown' href='#'><i class='ti-more'></i></a><div class='dropdown-menu dropdown-menu-right'><a class='dropdown-item' href='#'>Open</a><a class='dropdown-item active' href='#' data-navigation-target='contact-information'>Profile</a><a class='dropdown-item' href='#'>Add to archive</a><a class='dropdown-item' href='#'>Delete</a></div></div></div></div></li>";
            else
              html = html + "<li class='list-group-item conversation' data-conversation-id='"+ dt._id+"'><div><figure class='avatar'><img class='rounded-circle' src='dist/images/women_avatar5.jpg'></figure></div><div class='users-list-body'><h5>" + dt.groupName + "</h5><p>" + dt.description + "</p><div class='users-list-action action-toggle'><div class='dropdown'><a data-toggle='dropdown' href='#'><i class='ti-more'></i></a><div class='dropdown-menu dropdown-menu-right'><a class='dropdown-item' href='#'>Open</a><a class='dropdown-item active' href='#' data-navigation-target='contact-information'>Profile</a><a class='dropdown-item' href='#'>Add to archive</a><a class='dropdown-item' href='#'>Delete</a></div></div></div></div></li>";
            }
        })
        html = html + "</ul>";
        $('#list').append(html);
    });

    $(document).on('click', '.layout .content .sidebar-group .sidebar .list-group-item', function () {
        if (jQuery.browser.mobile) {
            $(this).closest('.sidebar-group').removeClass('mobile-open');
        }
    })
});
