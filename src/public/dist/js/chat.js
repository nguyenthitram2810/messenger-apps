$(document).ready(function (){
    $('#chatBar').submit(function (e) {
        e.preventDefault();
        var chat_body = $('.layout .content .chat .chat-body');
        let message = $('input[name="message"]').val(); 
        message = $.trim(message);
        
        if (message) {
          $('.layout .content .chat .chat-body .messages').append('<div class="message-item outgoing-message ' + '"><div class="message-content">' + message + '</div><div class="message-action">PM 14:25 ' + ('<i class="ti-check"></i>') + '</div></div>');
          chat_body.scrollTop(chat_body.get(0).scrollHeight, -1).niceScroll({
            cursorcolor: 'rgba(66, 66, 66, 0.20)',
            cursorwidth: "4px",
            cursorborder: '0px'
          }).resize();
          socket.emit('outgoing', {message});
          $('input[name="message"]').val(''); 
        }
        else {
          $('input[name="message"]').focus(); 
        }
    });
    socket.on('incoming', (data) => {
      let incomingMessage = data;
      $('.layout .content .chat .chat-body .messages').append('<div class="message-item ' + '"><div class="message-content">' + incomingMessage + '</div><div class="message-action">PM 14:25 ' + ('<i class="ti-check"></i>') + '</div></div>');
    });
    $(document).on('click', '.layout .content .sidebar-group .sidebar .list-group-item', function () {
        if (jQuery.browser.mobile) {
            $(this).closest('.sidebar-group').removeClass('mobile-open');
        }
    });
});