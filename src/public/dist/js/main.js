$(document).ready(function (){
    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
    
    $('#login-phone').submit(function (event) {
        event.preventDefault();
        let phoneNumber = $('input[name="phoneNumber"]').val();
        phoneNumber = '+84' + phoneNumber.substring(1,phoneNumber.length);
        const appVerifier = window.recaptchaVerifier;

      firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
      .then(function (confirmationResult) {
        window.confirmationResult = confirmationResult;
        $('#login-step1').css('display','none');
        $('#login-step2').css('display','block')

        $('#verify-phone-login').submit(function (event) {
          event.preventDefault();
          const codeUser = $('input[name="codeVerify"]').val()
          confirmationResult.confirm(codeUser).then(function (result) {
            const user = result.user;
            firebase.auth().currentUser.getIdToken(true).then(function(idToken) {
              const userInfo = {
                phoneNumber,
                idToken
              };  
            
              $.ajax({
                type:'POST',
                url:'/login-sms',
                data: userInfo,
                success : function(data) {
                  if(data == 'error') {
                    window.alert("Verify ID token fail!");
                  }
                  if(data == 'successful') {
                    window.alert("Welcome!");
                    window.location.replace('/');
                  }
                }
              });
            })
          }).catch(function (error) {  
            console.log(error);
          });
        })
      }).catch(function (error) {
        console.log(error);
      });
    });

    $('#register-phone').submit(function (event) {
      event.preventDefault();
      let phoneNumber = $('input[name="phoneNumber"]').val();
      phoneNumber = '+84' + phoneNumber.substring(1,phoneNumber.length);
      const appVerifier = window.recaptchaVerifier;
      const firstName = $('input[name="firstName"]').val(); 
      const lastName = $('input[name="lastName"]').val();
      
      firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
      .then(function (confirmationResult) {
        window.confirmationResult = confirmationResult;
        $('#register-step1').css('display','none');
        $('#register-step2').css('display','block')

        $('#verify-phone-register').submit(function (event) {
          event.preventDefault();
          const codeUser = $('input[name="codeVerify"]').val()
          confirmationResult.confirm(codeUser).then(function (result) { // compare codeUSer and code firebase
            // User signed in successfully.
            const user = result.user;
            const userInfo = {
                firstName,
                lastName,
                phoneNumber
            };
            
            $.ajax ({
              type:'POST',
              url:'/register-sms',
              data: userInfo,
              success: function() {
                window.alert("Welcome!");
                window.location.replace('/');
              }
            });
          }).catch(function (error) {
            console.log(error);
            
          });
        })
      }).catch(function (error) {
          console.log('error', error);
      });
    });

    $('#register-email').submit(function (event) {
      event.preventDefault(); 
      const email = $('input[name="email"]').val(); 
      const password = $('input[name="password"]').val();
      const firstName = $('input[name="firstName"]').val(); 
      const lastName = $('input[name="lastName"]').val();
      //firebase create
      firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(function(result) {
        var user = firebase.auth().currentUser; 
        const userInfo = {
          firstName,
          lastName,
          email,
          password
        }

        //user exist => send email verification 
        if(user) {
          user.sendEmailVerification().then(function() {
            $.ajax ({
              type:'POST',
              url:'/register-email',
              data: userInfo,
              //Success neu tra ve file res.json
              success : function() {
                window.alert("Register successful! Verify email before login!");
                window.location.replace("/login-email");
              }
            })
          }) 
        }
        else {
          window.alert('ERROR!')
        } 
      })
    });

    $('#signIn-email').submit(function (event) {
      event.preventDefault();
      const email = $('input[name="email"]').val(); 
      const password = $('input[name="password"]').val();

      firebase.auth().signInWithEmailAndPassword(email, password)
      .then(function(result) {
        const user = firebase.auth().currentUser; 
        if(user.emailVerified) {
          firebase.auth().currentUser.getIdToken(true).then(function(idToken) {
            const userInfo = {
              email,
              password,
              idToken,
            };  
          
            $.ajax({
              type:'POST',
              url:'/login-email',
              data: userInfo,
              success : function(data) {
                  if(data == 'error') {
                    window.location.replace('/login-email');
                    window.alert('Wrong ID token!');
                  }
                  if(data == 'successful') {
                    window.location.replace('/');
                    window.alert('Welcome!');
                  }
              }
            });
          });
        }
        else { 
          window.alert("Login email and verify!");
        }
      }).catch(function(error) {
          console.log(error.message);
      });
    });
});

$(document).ready(function() {
  $('#logout').on('show.bs.modal', function () {
    $('#settingModal').modal('hide');
  })
  function checkConversation() {
    if(window.location.pathname === '/') {
      if($('#conversations').hasClass('conversation')) {
        window.location.href = '/' + $('.conversation').first().data('conversation-id');
      }
    }
    else {
      $('.conversation').each(function() {
        if($(this).data('conversation-id') === window.location.pathname.replace('/','')) {
          $(this).addClass('open-chat');
          const conversationId = $('.open-chat').data('conversation-id');
          const userId = $('#Itsme').data('user');
          socket.emit('join', conversationId);
          socketUser.emit('join', userId);
        }
      });
    }    
  }
  checkConversation();

  $('#users').keyup(function(event) {
      event.preventDefault();
      $('#listUsers').remove();
      let fullName= new Array;
      fullName.push($('input[name="q"]').val());
      fullName = JSON.stringify(fullName);

      if(fullName[2] != '"') {
        $.ajax({
          type: 'POST',
          url: '/search-user',
          data: {
            fullName,
          },
          success: function(data) {
            $('.mainUser').remove();
            $('.listMember').append("<div class='user-id d-none mainUser' data-user-id = '"+data.objId[0].id+"' data-obj-id = '"+data.objId[0]._id+"'></div>");
            let html = "<ul id='listUsers' style='list-style-type:none; padding-left:0px; background:#EEEEEE;'>";
            data.user.forEach(function(dt) {
              let check = 0;
              $('.user-id').each(function() {
                if($(this).data('user-id') == dt.id) {
                  check =1;
                }
              });
              if(dt.id == data.objId[0].id) {
                check = 1;
              }
              if(check == 0) {
                html = html + "<span class='pl-3 d-flex align-items-center member' style='height:50px; cursor:pointer;' data-user-id = '" + dt.id + 
                "' data-obj-id='"+ dt._id +"'> <img class = 'rounded-circle mr-2' src = 'dist/images/man_avatar3.jpg' width='30px' height='30px'><li>" + dt.name+"</li></span>"; 
              }
            });
            html = html + "</ul>";
            $('#addMember').append(html);
          }
        })
      }
  });

  $(document).on('click','.member',function() {
    const id = $(this).data('user-id');
    const objId = $(this).data('obj-id');
    const name = $(this).text();
    let html = "<div class = 'mb-3 col-4 member-background d-flex align-items-center'><div class = 'w-auto user-id btn btn-success px-1' data-user-id = '"+ id + "' data-obj-id = '"+ objId +"'> "+ name +" <span class='removeMember'>×</span></div></div>";
    $('.listMember').append(html);
    $(this).remove();
  });

  $(document).on('click','.removeMember',function() {
    $(this).parent().parent().remove();
  });

  $('#createGroup').submit(function(event) {
    event.preventDefault();
    const groupName = $('input[name="groupName"]').val();
    const description = $('textarea[name="description"]').val();
    let users_ID = new Array;
    let users_objId = new Array;
    $('.user-id').each(function() {
      users_ID.push($(this).data('user-id'));
      users_objId.push($(this).data('obj-id'));
    });
    users_ID =  JSON.stringify(users_ID);
    users_objId = JSON.stringify(users_objId);
    $.ajax({
        type:'POST',
        url:'/group-add',
        data: {
          users_ID,
          users_objId,
          groupName,
          description,
        },
        success: function(data) {
            $('#newGroup').modal('hide');
            window.location.replace('/');
        },
    });
  });

  $('#listConversation').click(function(event) {
    event.preventDefault();
    $('#conversations').remove();
    $.ajax({
      type:'POST',
      url:'/list-conversation',
      success: function(data) {
        let html = "<ul id = 'conversations' class = 'list-group list-group-flush'>";
        data.conversations.forEach(function(dt) {
          if(dt.userIds.length == 2) {
            dt.userIds.forEach(function(ob) {
              if(data.ID != ob.id) {
                 html = html + "<li class='list-group-item conversation' data-conversation-id='"+ dt._id+"'><div><figure class='avatar'><img class='rounded-circle' src='dist/images/women_avatar5.jpg'></figure></div><div class='users-list-body'><h5>" + ob.name + "</h5><p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p><div class='users-list-action action-toggle'><div class='dropdown'><a data-toggle='dropdown' href='#'><i class='ti-more'></i></a><div class='dropdown-menu dropdown-menu-right'><a class='dropdown-item' href='#'>Open</a><a class='dropdown-item active' href='#' data-navigation-target='contact-information'>Profile</a><a class='dropdown-item' href='#'>Add to archive</a><a class='dropdown-item' href='#'>Delete</a></div></div></div></div></li>";
              }
            })
          }
          else {
            html = html + "<li class='list-group-item conversation' data-conversation-id='"+ dt._id+"'><div><figure class='avatar'><img class='rounded-circle' src='dist/images/women_avatar5.jpg'></figure></div><div class='users-list-body'><h5>" + dt.groupName + "</h5><p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p><div class='users-list-action action-toggle'><div class='dropdown'><a data-toggle='dropdown' href='#'><i class='ti-more'></i></a><div class='dropdown-menu dropdown-menu-right'><a class='dropdown-item' href='#'>Open</a><a class='dropdown-item active' href='#' data-navigation-target='contact-information'>Profile</a><a class='dropdown-item' href='#'>Add to archive</a><a class='dropdown-item' href='#'>Delete</a></div></div></div></div></li>";
          }
        })
        html = html + "</ul>";
        $('#list').append(html);
      },
    });
  })
  
  $(document).on('click','.conversation',function(event) {
    window.location.href='/' + $(this).data('conversation-id');
  });
  const messaging = firebase.messaging();
  messaging.usePublicVapidKey("BJnoRhpzlnjuLsVPgmCHoxbHh711UVot85CNzgm6n2UKvStKOu4JbtbvQ6j3pE0A9iGd90y_YbxO5CtSiqDX8L8");
  Notification.requestPermission().then((permission) => {
    if (permission === 'granted') {
      messaging.getToken().then((currentToken) => {
        if (currentToken) {
          console.log(currentToken);
          
        } else {
          // Show permission request.
          console.log('No Instance ID token available. Request permission to generate one.');

        }
      }).catch((err) => {
        console.log('An error occurred while retrieving token. ', err);

      });
    } else {
      console.log('Unable to get permission to notify.');
    }
  });

  messaging.setBackgroundMessageHandler(function(payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    const notificationTitle = 'Background Message Title';
    const notificationOptions = {
      body: 'Background Message body.',
      icon: '/firebase-logo.png'
    };
  
    return self.registration.showNotification(notificationTitle,
      notificationOptions);
  });

  // messaging.onMessage((payload) => {
  //   console.log('Message received. ', payload);
  //   // ...
  // });
})