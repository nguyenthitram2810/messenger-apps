exports.up = (knex) => knex.schema.createTable('friends', (table) => {
    table.increments('id').primary();
    table.integer('userId').unsigned();
    table.foreign('userId').references('id').inTable('users').onDelete('CASCADE');
    table.integer('friendId').unsigned();
    table.foreign('friendId').references('id').inTable('users').onDelete('CASCADE');
    table.integer('receiverId');
    table.integer('status');
    table.timestamp('createdAt').defaultTo(knex.fn.now());
    table.timestamp('updatedAt').defaultTo(knex.fn.now());
  });

  exports.down = (knex) => knex.schema.dropTableIfExists('friends');
