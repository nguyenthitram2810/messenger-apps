import mongoose from 'mongoose';

const { Schema } = mongoose;

const schema = new mongoose.Schema({
  content: {
    type: String,
    required: true,
    trim: true,
  },
  conversationID: [{
    type: Schema.Types.ObjectId,
    ref: 'conversation', // tham chieu toi bang conversation
  }],
  memberId: [{
    type: Schema.Types.ObjectId,
    ref: 'users',
  }],
}, {
  timestamps: true,
});
export default mongoose.model('messages', schema);

// Models user
// id
// name 
// avatar 
// .populate('ten key) vd: .populate('ids','name');
