import mongoose from 'mongoose';

const { Schema } = mongoose;

const schema = new mongoose.Schema({
  groupName: String,
  userIds: [{
    type: Schema.Types.ObjectId,
    ref: 'users',
  }],
  description: String,
  member: [Number],
}, {
  timestamps: true,
});
export default mongoose.model('conversation', schema);
