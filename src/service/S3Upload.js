import AWS from 'aws-sdk';
import fs from 'fs';

require('dotenv').config();

const accessKeyId = process.env.AWS_ACCESS_KEY;
const secretAccessKey = process.env.AWS_SECRET_KEY;

AWS.config.update({
    accessKeyId,
    secretAccessKey,
    region: 'ap-southeast-1',
});

const s3 = new AWS.S3();

const s3Upload = async (file) => {
    const params = {
        Bucket: 'sgroupit-test',
        Key: `trambucket/${file.filename.toLowerCase()}`,
        Body: fs.readFileSync(`${file.path}`),
        ContentType: file.mimetype,
        ACL: 'public-read',
    };
    const data = await s3.upload(params).promise();
    fs.unlinkSync(`${file.path}`);
    return data;
};

export default {
    s3Upload,
};
